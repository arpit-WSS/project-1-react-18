import { SearchCountry } from './components/SearchCountry/SearchCountry';
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Information } from './components/Information/Information';

import { AppBarComponent } from './components/AppBarComponent/AppBarComponent';

function App() {
	return (
		<BrowserRouter>
			<AppBarComponent />
			<Routes>
				<Route path="/" element={<SearchCountry />} />
				<Route path="/:countryName" element={<Information />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
