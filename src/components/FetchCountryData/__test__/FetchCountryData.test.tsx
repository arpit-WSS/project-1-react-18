import '@testing-library/jest-dom';
import { render, cleanup, screen, fireEvent } from '@testing-library/react';
import user from '@testing-library/user-event';

import { BrowserRouter } from 'react-router-dom';
import { FetchCountryData } from '../FetchCountryData';

const mockedclick = jest.fn();

const mockeddata = {
	name: [
		{
			capital: [ 'New Delhi' ],
			population: 1380004385,
			flags: { png: 'https://flagcdn.com/w320/in.png' },
			latlng: [ 20, 77 ]
		}
	]
};

describe('', () => {
	beforeEach(() => {
		function tree() {
			return render(
				<FetchCountryData data={mockeddata} onclick={mockedclick} loading={false} countryname={'India'} />,
				{ wrapper: BrowserRouter }
			);
		}
		const { findByTestId } = tree();
	});

	afterEach(() => {
		cleanup();
	});

	it('[Test-1]', async () => {
		const elem = await screen.findByText('Population: 1380004385');
		expect(elem).toBeInTheDocument();
	});

	it('[Test-2]', async () => {
		const button = await screen.findByTestId('CapitalData');
		fireEvent.click(button);
	});

	it('[Test-3]', async () => {
		const elems = await screen.findAllByTestId('textelem');
		expect(elems).toHaveLength(5);
	});
});
