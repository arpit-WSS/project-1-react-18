import React from 'react';
import { Grid, Divider, Button, Card, CardMedia, CardContent, Typography, CardActions } from '@mui/material';

interface Props {
	data: any;
	loading: boolean;
	countryname: string | undefined;
	onclick: () => void;
}

export const FetchCountryData = (props: Props) => {
	return (
		<Grid container item md={6} xs={12} justifyContent="center" alignItems="center">
			{!props.loading ? (
				props.data.name.map((cdata: any, index: number) => (
					<Card key={index} variant="outlined" sx={{ minWidth: '450px', background: '#d8e2dc' }}>
						<CardContent>
							<Typography data-testid="textelem" mb={2}>
								Country: {props.countryname}
							</Typography>

							<Typography data-testid="textelem" mb={2}>
								Capital: {cdata['capital'][0]}
							</Typography>
							<Typography data-testid="textelem" mb={2}>
								Population: {cdata['population']}
							</Typography>
							<Typography data-testid="textelem" mb={2}>
								Latitude: {cdata['latlng'][0]}
							</Typography>
							<Typography data-testid="textelem" mb={2}>
								Longitude: {cdata['latlng'][1]}
							</Typography>
							<CardMedia component="img" image={cdata['flags']['png']} height="200" alt="country flag" />
						</CardContent>
						<CardActions>
							<Button
								data-testid="CapitalData"
								variant="contained"
								onClick={() => {
									props.onclick();
								}}
							>
								Capital Data
							</Button>
						</CardActions>
					</Card>
				))
			) : null}
		</Grid>
	);
};
