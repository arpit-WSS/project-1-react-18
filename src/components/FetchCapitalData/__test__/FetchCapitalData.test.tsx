import '@testing-library/jest-dom';
import { render, cleanup, screen, fireEvent } from '@testing-library/react';
import user from '@testing-library/user-event';

import { BrowserRouter } from 'react-router-dom';
import { FetchCapitalData } from '../FetchCapitalData';

describe('', () => {
	beforeEach(() => {
		function tree() {
			return render(<FetchCapitalData isCapLoading={true} capitalname={'New Delhi'} />, {
				wrapper: BrowserRouter
			});
		}
		const { findByTestId } = tree();
	});

	afterEach(() => {
		cleanup();
	});

	it('[Test-1]', async () => {
		const elem = await screen.findByText('Precipitation: 0 mm');
		expect(elem).toBeInTheDocument();
	});

	it('[Test-2]', async () => {
		const elems = await screen.findAllByTestId('textelem');
		expect(elems).toHaveLength(4);
	});
});
