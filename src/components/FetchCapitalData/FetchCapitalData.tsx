import React, { useState, useEffect } from 'react';
import { Grid, Card, CardMedia, CardContent, Typography, Divider } from '@mui/material';
import { API_KEY } from '../../config';

interface AnyData {
	capdata: any;
}

const initialState: AnyData = {
	capdata: []
};
interface Props {
	isCapLoading: boolean;
	capitalname: string;
}
export const FetchCapitalData = (props: Props) => {
	const [ cdata, setcData ] = useState(initialState);
	const [ loading, setloading ] = useState(false);
	const [ error, setError ] = useState(false);

	const formatedCapName = props.capitalname.split(' ').join('%20');
	const capitalEndpoint = `http://api.weatherstack.com/current?access_key=${API_KEY}&query=${formatedCapName}`;

	useEffect(
		() => {
			const fetchData = async () => {
				try {
					const capitaldata = await fetch(capitalEndpoint).then((res) => res.json());
					console.log(capitaldata.current);
					setcData({ capdata: [ { ...capitaldata.current } ] });
				} catch (error) {
					setError(true);
					setloading(false);
				}
				setloading(false);
			};
			setcData(initialState);
			if (props.isCapLoading) {
				fetchData();
			}
		},
		[ props.isCapLoading ]
	);

	return (
		<Grid container item md={6} xs={12} justifyContent="center" alignItems="center">
			{props.isCapLoading ? (
				cdata.capdata.map((item: any, index: number) => (
					<Card key={index} variant="outlined" sx={{ minWidth: '450px', backgroundColor: '#d8e2dc' }}>
						<CardContent>
							<Typography data-testid="textelem" variant="h5">
								Capital: {props.capitalname}
							</Typography>
							<Divider />

							<CardMedia component="img" image={item['weather_icons']} height="200" alt="country flag" />
							<Typography data-testid="textelem" my={2}>
								Temperature: {item['temperature']} °C
							</Typography>
							<Typography data-testid="textelem" my={2}>
								Precipitation: {item['precip']} mm
							</Typography>
							<Typography data-testid="textelem" mt={2}>
								Wind Speed: {item['wind_speed']} m/hr
							</Typography>
						</CardContent>
					</Card>
				))
			) : null}
		</Grid>
	);
};
