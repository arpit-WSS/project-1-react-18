import React from 'react';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { Grid } from '@mui/material';
import { FetchCountryData } from '../FetchCountryData/FetchCountryData';
import { FetchCapitalData } from '../FetchCapitalData/FetchCapitalData';

interface AnyData {
	name: any;
}

const inititalState: AnyData = {
	name: []
};

export const Information = () => {
	const [ data, setData ] = useState(inititalState);
	const [ loading, setloading ] = useState(false);
	const [ error, setError ] = useState(false);
	const { countryName } = useParams();
	const [ isCapLoading, setisCapLoading ] = useState(false);
	const [ getCapName, setgetCapName ] = useState('');

	const coutnryEndPoint = `https://restcountries.com/v3.1/name/${countryName}`;

	useEffect(() => {
		const fetchData = async () => {
			try {
				setloading(true);
				const countrydata = await fetch(coutnryEndPoint).then((res) => res.json());
				setData({ name: [ ...countrydata ] });
			} catch (error) {
				setError(true);
				setloading(false);
			}
			setloading(false);
		};

		fetchData();
	}, []);

	const onclick = () => {
		setisCapLoading(true);
		data.name.map((cdata: any, index: number) => {
			const cname = cdata['capital'][0];
			setgetCapName(cname);
		});
		console.log('button clicked');
	};

	return (
		<Grid container item xs={12} sx={{ minHeight: '720px' }} justifyContent="center" alignItems="center">
			<FetchCountryData data={data} loading={loading} countryname={countryName} onclick={onclick} />
			<FetchCapitalData isCapLoading={isCapLoading} capitalname={getCapName} />
		</Grid>
	);
};
