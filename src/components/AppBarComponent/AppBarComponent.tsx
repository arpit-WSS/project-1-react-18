import React from 'react';
import { AppBar, Toolbar, Typography } from '@mui/material';

export const AppBarComponent = () => {
	return (
		<AppBar position="sticky">
			<Toolbar>
				<Typography variant="h5" align="center" sx={{ width: '100%' }}>
					Country Data
				</Typography>
			</Toolbar>
		</AppBar>
	);
};
