import React, { useState, useMemo } from 'react';
import { Button, Grid, TextField, Typography, Autocomplete } from '@mui/material';
import worldmap from '../../Images/worldmap.jpg';
import { useNavigate } from 'react-router-dom';

export const SearchCountry = () => {
	const [ countryName, setCountryName ] = useState('');
	const navigate = useNavigate();

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		e.preventDefault();
		setCountryName(e.target.value);
	};

	const handleSubmit = (e: React.MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();
		navigate(`./${countryName}`);
	};

	return (
		<Grid container item md={12} justifyContent="center" alignItems="center">
			<Grid container item md={6} xs={12} justifyContent="center" alignItems="center" direction="column">
				<img src={worldmap} />
			</Grid>
			<Grid item container md={6} xs={12} justifyContent="center" alignItems="center" direction="column">
				<TextField
					variant="outlined"
					margin="normal"
					size="small"
					label="Enter Country"
					onChange={handleChange}
					data-testid="textfield"
					helperText="Please Enter correct/full name of the country"
				/>

				<Button
					data-testid="submitbutton"
					variant="contained"
					disabled={countryName.length < 1}
					onClick={(e) => handleSubmit(e)}
				>
					Submit Button
				</Button>
			</Grid>
		</Grid>
	);
};
