import '@testing-library/jest-dom';
import { render, cleanup, screen, fireEvent } from '@testing-library/react';
import user from '@testing-library/user-event';

import { BrowserRouter } from 'react-router-dom';
import { SearchCountry } from '../SearchCountry';

describe('', () => {
	beforeEach(() => {
		render(<SearchCountry />, { wrapper: BrowserRouter });
	});

	afterEach(() => {
		cleanup();
	});

	it('Check if button is disabled', () => {
		const button = screen.getByTestId('submitbutton');
		expect(button).toHaveAttribute('disabled');
	});

	it('Check if button is enabled', async () => {
		const button = screen.getByTestId('submitbutton').closest('button');
		expect(button).toBeDisabled();
		const inputfield = screen.getByRole('textbox', { name: /Enter Country/i });
		user.type(inputfield, 'india');
		expect(button).not.toBeDisabled();
		// expect(button).toHaveProperty('disabled', false);

		//button click
		fireEvent(screen.getByTestId('submitbutton'), new MouseEvent('click', { bubbles: true }));
	});
});
